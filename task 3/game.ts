let rows = 20;
let columns = 20;
let maxImgs = rows * columns - 5;
let currentImgsCount = 0;

let busyCells: number[][] = [];

startGame();

function startGame() {
    if (currentImgsCount + 5 < maxImgs) {
        currentImgsCount += 5;
    }

    clearSmilies();
    busyCells.splice(0, busyCells.length);
    
    for (let i = 0; i < currentImgsCount; i++) {
        generateImg();
    }

    generateAnomaly();
}

function generateImg() {
    let img = createSmileyImg(generateRandomPosition());
    document.getElementById("left-side").appendChild(img);
    document.getElementById("right-side").appendChild(img.cloneNode(true));
}

function createSmileyImg(position: number[]): HTMLImageElement {
    let img = document.createElement('img');
    img.src = "smiley.png";
    img.classList.add("smiley");
    img.style.gridArea = position[0] + "/" + position[1] + "/" + (position[0] + 1) + "/" + (position[1] + 1);

    return img;
}

function generateAnomaly() {
    let img = createSmileyImg(generateRandomPosition());
    img.onclick = () => {
        let harder = confirm("WOOOOW YOU MADE IT !! Wanna try it harder ?");
        (harder) ? startGame() : null;
    };

    document.getElementById("left-side").appendChild(img);
}

function generateRandomPosition(): number[] {
    let randomTop: number;
    let randomLeft: number;
    do {
        randomTop = Math.floor(Math.random() * rows);
        randomLeft = Math.floor(Math.random() * columns);
    }
    while (checkDuplicate([randomTop, randomLeft]));

    return [randomTop, randomLeft];
}

function checkDuplicate(candidate: number[]): boolean {
    for (let cell of busyCells) {
        if (cell[0] === candidate[0]) {
            if (cell[1] === candidate[1]) {
                return true;
            }
        }
    }
    return false;
}

function clearSmilies() {
    let leftSide = document.getElementById("left-side");
    let rightSide = document.getElementById("right-side");

    while (leftSide.firstChild) {
        leftSide.removeChild(leftSide.firstChild);
    }

    while (rightSide.firstChild) {
        rightSide.removeChild(rightSide.firstChild);
    }
}