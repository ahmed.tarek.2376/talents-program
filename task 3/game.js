var rows = 20;
var columns = 20;
var maxImgs = rows * columns - 5;
var currentImgsCount = 0;
var busyCells = [];
startGame();
function startGame() {
    if (currentImgsCount + 5 < maxImgs) {
        currentImgsCount += 5;
    }
    clearSmilies();
    for (var i = 0; i < currentImgsCount; i++) {
        generateImg();
    }
    generateAnomaly();
}
function generateImg() {
    var img = createSmileyImg(generateRandomPosition());
    document.getElementById("left-side").appendChild(img);
    document.getElementById("right-side").appendChild(img.cloneNode(true));
}
function createSmileyImg(position) {
    var img = document.createElement('img');
    img.src = "smiley.png";
    img.classList.add("smiley");
    img.style.gridArea = position[0] + "/" + position[1] + "/" + (position[0] + 1) + "/" + (position[1] + 1);
    return img;
}
function generateAnomaly() {
    var img = createSmileyImg(generateRandomPosition());
    img.onclick = function () {
        var harder = confirm("WOOOOW YOU MADE IT !! Wanna try it harder ?");
        (harder) ? startGame() : null;
    };
    document.getElementById("left-side").appendChild(img);
}
function generateRandomPosition() {
    var randomTop;
    var randomLeft;
    do {
        randomTop = Math.floor(Math.random() * rows);
        randomLeft = Math.floor(Math.random() * columns);
    } while (checkDuplicate([randomTop, randomLeft]));
    return [randomTop, randomLeft];
}
function checkDuplicate(candidate) {
    for (var _i = 0, busyCells_1 = busyCells; _i < busyCells_1.length; _i++) {
        var cell = busyCells_1[_i];
        if (cell[0] === candidate[0]) {
            if (cell[1] === candidate[1]) {
                return true;
            }
        }
    }
    return false;
}
function clearSmilies() {
    var leftSide = document.getElementById("left-side");
    var rightSide = document.getElementById("right-side");
    while (leftSide.firstChild) {
        leftSide.removeChild(leftSide.firstChild);
    }
    while (rightSide.firstChild) {
        rightSide.removeChild(rightSide.firstChild);
    }
}
