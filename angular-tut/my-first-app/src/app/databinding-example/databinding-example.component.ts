import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-databinding-example',
  templateUrl: './databinding-example.component.html',
  styleUrls: ['./databinding-example.component.scss']
})
export class DatabindingExampleComponent {
  user = "";
  isDisabled = true;
  statusMssg = "This is the default and will not be shown";
  userCreated = false;

  constructor() {
  }

  createUser(){
    this.statusMssg = `User ${this.user} created`;
    this.userCreated = true;
    this.user = "";
  }

  onUpdateUser(){
    if(this.user.length===0){
      this.isDisabled = true;
    } else {
      this.isDisabled = false;
    }
  }

}
