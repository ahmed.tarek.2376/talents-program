import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives-example',
  templateUrl: './directives-example.component.html',
  styleUrls: ['./directives-example.component.scss'],
})
export class DirectivesExampleComponent {
  clicksLog: String[] = [];
  clickCount = 0;

  logButton() {
    this.clicksLog.push((++this.clickCount).toString());
  }
}
