import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss'],
})
export class RecipeListComponent {
  @Output() recipeSelectedEvent = new EventEmitter<Recipe>();
  recipes: Recipe[] = [];

  constructor() {
    this.recipes = [
      new Recipe(
        'Test Recipe',
        'This is simply a test recipe',
        'https://images.immediate.co.uk/production/volatile/sites/30/2020/08/processed-food700-350-e6d0f0f.jpg'
      ),
      new Recipe(
        'Test Recipe',
        'This is simply a test recipe',
        'https://images.immediate.co.uk/production/volatile/sites/30/2020/08/processed-food700-350-e6d0f0f.jpg'
      ),
    ];
  }

  onRecipeSelected(recipe: Recipe) {
    this.recipeSelectedEvent.emit(recipe);
    console.log('recipe-list component : onRecipeSelected');
  }
}
