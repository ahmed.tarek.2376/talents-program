import { map } from 'rxjs/operators';
import { Movie } from '../shared/movie.model';

import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, merge, BehaviorSubject } from 'rxjs';
import { TopMoviesService } from '../services/top-movies.service';

// const EXAMPLE_DATA: Movie[] = [
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.6,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 3.9,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.0,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.9,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.9,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.9,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.9,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.9,
//   },
// ];

export class TopMoviesSDataSource extends DataSource<Movie> {
  data = new BehaviorSubject<Movie[]>([]);
  // Using BehaviorSubject is a great way of writing code that works independently
  // of the order that we use to perform asynchronous operations such as: calling the backend,
  // binding the data table to the data source, etc.

  private loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();

  paginator: MatPaginator | undefined;
  sort: MatSort | undefined;

  totalPopularMovies = 0;

  constructor(private movieService: TopMoviesService) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<Movie[]> {
    if (this.paginator) {
      // Combine everything that affects the rendered data into one update
      // stream for the data-table to consume.
      return merge(this.data, this.paginator.page).pipe(
        map(() => {
          return this.getPagedData(this.data.getValue());
        })
      );
    } else {
      throw Error(
        'Please set the paginator and sort on the data source before connecting.'
      );
    }
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect(): void {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: Movie[]): Movie[] {
    this.loadingSubject.next(true);

    if (this.paginator) {
      const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
      console.log(data.length, startIndex);
      if (data.length > startIndex) {
        this.loadingSubject.next(false);
        return data.splice(startIndex, this.paginator.pageSize);
      } else {
        this.loadPopularMovies();
        return [];
      }
    } else {
      return [];
    }
  }

  // private getSortedData(data: Movie[]): Movie[] {
  //   console.log('getSortedData', data);
  //   if (!this.sort || !this.sort.active || this.sort.direction === '') {
  //     return data;
  //   }

  //   return data.sort((a, b) => {
  //     const isAsc = this.sort?.direction === 'asc';
  //     switch (this.sort?.active) {
  //       case 'movie':
  //         return compare(a.title, b.title, isAsc);
  //       case 'vote-average':
  //         return compare(a.vote_average, b.vote_average, isAsc);
  //       case 'popularity':
  //         return compare(a.popularity, b.popularity, isAsc);
  //       default:
  //         return 0;
  //     }
  //   });
  // }

  loadPopularMovies() {
    const currentPage = this.paginator ? this.paginator.pageIndex + 1 : 1;

    if (currentPage * 20 + 1 > this.data.getValue().length)
      this.movieService.fetchPopularMovies(currentPage).subscribe((movies) => {
        const currentMovies = this.data.getValue();

        if (currentPage != 1) {
          currentMovies.push(...movies);
        } else if (currentMovies.length == 0) {
          //to avoid pushing the first 20 movies twice
          currentMovies.push(...movies);
        }

        this.data.next(currentMovies);
        this.totalPopularMovies = this.movieService.totalPopularMovies;
        console.log('loadPopularMovies callback', this.data.getValue());
      });
  }

  // loadLocalMovies() {
  //   this.data.next(EXAMPLE_DATA);
  // }
}

// function compare(
//   a: string | number,
//   b: string | number,
//   isAsc: boolean
// ): number {
//   return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
// }
