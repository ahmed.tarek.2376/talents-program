import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { TopMoviesService } from '../services/top-movies.service';
import { Movie } from '../shared/movie.model';
import { TopMoviesSDataSource } from './top-movies.datasource';

@Component({
  selector: 'app-top-movies',
  templateUrl: './top-movies.component.html',
  styleUrls: ['./top-movies.component.scss'],
})
export class TopMoviesComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  // @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<Movie>;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['poster', 'movie', 'popularity', 'vote-average'];

  dataSource: TopMoviesSDataSource;

  constructor(
    private authService: AuthService,
    private router: Router,
    private moviesService: TopMoviesService
  ) {
    this.dataSource = new TopMoviesSDataSource(moviesService);
  }

  ngOnInit(): void {
    this.dataSource.loadPopularMovies();
  }

  ngAfterViewInit(): void {
    // this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/']);
  }

  onMovieClicked(movie: Movie) {
    this.router.navigate(['movie', movie.id]);
  }
}
