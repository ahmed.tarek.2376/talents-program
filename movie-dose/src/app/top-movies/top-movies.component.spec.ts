// import { HttpClient, HttpClientModule } from '@angular/common/http';
// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { MatTable } from '@angular/material/table';
// import { Router, RouterModule } from '@angular/router';
// import { RouterTestingModule } from '@angular/router/testing';
// import { BehaviorSubject } from 'rxjs';
// import { AuthService } from '../auth/auth.service';
// import { Movie } from '../shared/movie.model';

// import { TopMoviesComponent } from './top-movies.component';
// import { TopMoviesSDataSource } from './top-movies.datasource';
// import { TopMoviesService } from '../services/top-movies.service';

// class abc {
//   data: Movie[] = [
//     {
//       poster_path:
//         'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//       overview:
//         "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//       release_date: '2019',
//       id: '123',
//       original_title: 'Joker',
//       title: 'Joker',
//       popularity: 12,
//       vote_average: 7.6,
//       vote_count: 123,
//     },
//   ];
//   fetchPopularMovies(page: number) {
//     return new Promise((res, rej) => {
//       res(this.data);
//     });
//   }
// }

// class DataSourceMock {
//   data = new BehaviorSubject<Movie[]>([
//     {
//       poster_path:
//         'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//       overview:
//         "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//       release_date: '2019',
//       id: '123',
//       original_title: 'Joker',
//       title: 'Joker',
//       popularity: 12,
//       vote_average: 7.6,
//       vote_count: 123,
//     },
//   ]);
//   // Using BehaviorSubject is a great way of writing code that works independently
//   // of the order that we use to perform asynchronous operations such as: calling the backend,
//   // binding the data table to the data source, etc.

//   // private loadingSubject = new BehaviorSubject<boolean>(false);
//   // public loading$ = this.loadingSubject.asObservable();

//   // paginator: MatPaginator | undefined;
//   // sort: MatSort | undefined;

//   // totalPopularMovies = 0;

//   loadPopularMovies() {}
// }

// describe('TopMoviesComponent', () => {
//   let component: TopMoviesComponent;
//   let fixture: ComponentFixture<TopMoviesComponent>;
//   let moviesService: TopMoviesService;
//   let authService: AuthService;
//   let myDataSource: TopMoviesSDataSource;

//   // let router: Router;

//   beforeEach(async () => {
//     await TestBed.configureTestingModule({
//       declarations: [TopMoviesComponent],
//       imports: [
//         RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }),
//         HttpClientModule,
//       ],
//       providers: [
//         AuthService,
//         { provide: TopMoviesSDataSource, useClass: DataSourceMock },
//         TopMoviesService,
//         // { provide: MatTable, useValue: {} },
//         // TopMoviesSDataSource,
//       ],
//     }).compileComponents();

//     // moviesService = TestBed.inject(TopMoviesService);
//   });

//   beforeEach(() => {
//     fixture = TestBed.createComponent(TopMoviesComponent);
//     component = fixture.componentInstance;

//     myDataSource = TestBed.inject(TopMoviesSDataSource);

//     spyOn(myDataSource, 'loadPopularMovies').and.returnValue();
//     // myDataSource.data.next([
//     //   {
//     //     poster_path:
//     //       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     //     overview:
//     //       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     //     release_date: '2019',
//     //     id: '123',
//     //     original_title: 'Joker',
//     //     title: 'Joker',
//     //     popularity: 12,
//     //     vote_average: 7.6,
//     //     vote_count: 123,
//     //   },
//     // ]);

//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
