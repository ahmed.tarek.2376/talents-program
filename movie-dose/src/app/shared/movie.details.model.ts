export interface MovieDetails {
  id: number;
  imdb_id: number | null;
  original_title: string;
  title: string;
  popularity: number;
  vote_average: number;
  vote_count: number;
  release_date: string;
  poster_path: string;
  overview: string;
  genres: [Genre];
  backdrop_path: string | null;
}

interface Genre {
  id: number;
  name: string;
}
