export interface Movie {
  poster_path: string;
  overview: string;
  release_date: string;
  id: string;
  original_title: string;
  title: string;
  popularity: number;
  vote_average: number;
  vote_count: number;
}
