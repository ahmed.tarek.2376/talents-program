import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { User } from '../shared/user.model';

@Injectable()
export class AuthService {
  users: User[] = [
    new User('atarek@sumerge.com', 'Sumerge2020'),
    new User('sazouz@Sumerge.com', 'Sumerge2020'),
    new User('naraby@Sumerge.com', 'Sumerge2020'),
  ];

  currentUser: User | null = null;

  isLoggedInSubject = new BehaviorSubject<boolean>(false);
  isLoggedIn$ = this.isLoggedInSubject.asObservable();
  isLoggedIn: boolean = false;

  constructor(private router: Router) {
    this.isLoggedIn$.subscribe((bool: boolean) => (this.isLoggedIn = bool));
    this.isAuthenticated();
  }

  isAuthenticated() {
    if (!this.isLoggedIn) {
      console.log('not logged in - trying to load local');
      this.loadLocalUser();
    }

    return this.isLoggedIn;
  }

  isAuthenticatedObservable() {
    this.isAuthenticated;
    return this.isLoggedIn$;
  }

  login(user: User): boolean {
    for (let item of this.users) {
      if (item.username == user.username && item.password == user.password) {
        // this.isLoggedIn = true;
        this.isLoggedInSubject.next(true);
        this.currentUser = user;
        this.storeLocally(user);
      }
    }
    return this.isLoggedIn;
  }

  storeLocally(user: User) {
    localStorage.setItem('currentUser', JSON.stringify(user.username));
  }

  loadLocalUser() {
    const userData = localStorage.getItem('currentUser');
    if (!userData) {
      return;
    } else {
      const currentUser: { username: string } = JSON.parse(userData);
      // this.isLoggedIn = true;
      this.isLoggedInSubject.next(true);
      this.currentUser = new User(currentUser.username, '');
    }
  }

  logout() {
    // this.isLoggedIn = false;
    this.isLoggedInSubject.next(false);
    this.currentUser = null;
    this.deleteLocally();
    this.router.navigate(['']);
  }

  deleteLocally() {
    localStorage.removeItem('currentUser');
  }
}
