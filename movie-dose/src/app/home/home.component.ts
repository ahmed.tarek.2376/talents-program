import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  isLoggedIn = false;

  constructor(private authService: AuthService, private router: Router) {
    this.authService
      .isAuthenticatedObservable()
      .subscribe((bool: boolean) => (this.isLoggedIn = bool));
    // this.isLoggedIn = this.authService.isAuthenticated();
  }

  ngOnInit(): void {}

  login() {
    this.router.navigate(['login']);
  }

  logout() {
    this.authService.logout();
  }

  browse() {
    this.router.navigate(['popular-movies']);
  }
}
