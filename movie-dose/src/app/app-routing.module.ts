import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth-guard.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MovieComponent } from './movie/movie.component';
import { PopularMoviesComponent } from './popular-movies/popular-movies.component';
import { TopMoviesComponent } from './top-movies/top-movies.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  // {
  //   path: 'top-movies',
  //   canActivate: [AuthGuard],
  //   component: TopMoviesComponent,
  // },
  {
    path: 'popular-movies',
    canActivate: [AuthGuard],
    component: PopularMoviesComponent,
  },
  { path: 'movie/:id', canActivate: [AuthGuard], component: MovieComponent },
  // { path: 'table', component: TableDemoComponent },
  { path: '**', redirectTo: '' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
