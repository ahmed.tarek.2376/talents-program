import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { MovieDetails } from '../shared/movie.details.model';

@Injectable()
export class MovieDetailsService {
  constructor(private http: HttpClient) {}

  private apiKey = '127ef8102efbb5a087762506ab8cabc2';
  detailsURL = 'https://api.themoviedb.org/3/movie/';

  data: MovieDetails | undefined;
  totalPopularMovies = 0;

  fetchMovieDetails(id: number) {
    return this.http
      .get<MovieDetails>(this.detailsURL + id, {
        params: new HttpParams().set('api_key', this.apiKey),
      })
      .pipe(
        map((res) => {
          const details: MovieDetails = {
            ...res,
            poster_path: 'https://image.tmdb.org/t/p/w500' + res.poster_path,
            backdrop_path:
              'https://image.tmdb.org/t/p/w500' + res.backdrop_path,
            vote_average: res.vote_average,
          };

          return details;
        })
      );
  }
}
