import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieDetails } from '../shared/movie.details.model';
import { MovieDetailsService } from './movie.details.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss'],
})
export class MovieComponent implements OnInit {
  movieID: number = 0;
  details: MovieDetails = {
    id: 0,
    imdb_id: null,
    original_title: '',
    title: '',
    popularity: 0,
    vote_average: 0,
    vote_count: 0,
    release_date: '',
    poster_path: '',
    overview: '',
    genres: [{ id: 1, name: '' }],
    backdrop_path: null,
  };

  constructor(
    private route: ActivatedRoute,
    private detailsService: MovieDetailsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.movieID = this.route.snapshot.params['id'];
    this.detailsService.fetchMovieDetails(this.movieID).subscribe((details) => {
      this.details = details;
      console.log(details);
    });
  }

  navigatePopular() {
    this.router.navigate(['/popular-movies']);
  }
}
