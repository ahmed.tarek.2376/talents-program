import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { Observable } from 'rxjs';
import { TopMoviesService } from '../services/top-movies.service';
import { Movie } from '../shared/movie.model';

import { PopularMoviesComponent } from './popular-movies.component';

class MockMoviesService {
  fetchPopularMovies(page: number): Observable<Movie[]> {
    let obs = new Observable<Movie[]>((observer) => {
      if (page == 1) {
        observer.next(mockPage1);
      } else {
        observer.next(mockPage2);
      }
    });

    return obs;
  }
}

let mockPage1: Movie[] = [
  {
    poster_path:
      'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
    overview:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    release_date: '2019',
    id: '123',
    original_title: 'Joker',
    title: 'Joker',
    popularity: 12,
    vote_average: 7.6,
    vote_count: 123,
  },
];

let mockPage2: Movie[] = [
  {
    poster_path: 'https://i.pinimg.com/originals/eb/b9/40/.jpg',
    overview:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    release_date: '2030',
    id: '4567',
    original_title: 'Alice',
    title: 'Alice',
    popularity: 12,
    vote_average: 7.6,
    vote_count: 123,
  },
];

describe('PopularMoviesComponent', () => {
  let component: PopularMoviesComponent;
  let fixture: ComponentFixture<PopularMoviesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PopularMoviesComponent],
      imports: [RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      providers: [{ provide: TopMoviesService, useClass: MockMoviesService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopularMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update movies data after service call', () => {
    component.ngOnInit();
    expect(component.moviesData).toEqual(mockPage1);
  });

  it('should update movies data when updating paginator value', () => {
    component.handlePageEvent({ pageIndex: 2, pageSize: 20, length: 10000 });
    expect(component.moviesData).toEqual(mockPage2);
  });

  it('should update image url after fetching data', () => {
    component.ngOnInit();
    const myElement: HTMLElement = fixture.nativeElement;
    const movieImageElement: HTMLImageElement | null =
      myElement.querySelector('.poster-img');
    expect(movieImageElement?.src).toEqual(mockPage1[0].poster_path);
  });
});
