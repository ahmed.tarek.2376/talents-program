import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { TopMoviesService } from '../services/top-movies.service';
import { Movie } from '../shared/movie.model';

@Component({
  selector: 'app-popular-movies',
  templateUrl: './popular-movies.component.html',
  styleUrls: ['./popular-movies.component.scss'],
})
export class PopularMoviesComponent implements OnInit {
  // moviesData: Movie[]= [
  //   {
  //     poster_path:
  //       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
  //     overview:
  //       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
  //     release_date: '23-08-2019',
  //     id: '123',
  //     original_title: 'Joker',
  //     title: 'Joker',
  //     popularity: 12,
  //     vote_average: 7.6,
  //     vote_count: 4590,
  //   },
  //   {
  //     poster_path:
  //       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
  //     overview:
  //       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
  //     release_date: '23-08-2019',
  //     id: '123',
  //     original_title: 'Joker',
  //     title: 'Joker',
  //     popularity: 12,
  //     vote_average: 7.6,
  //     vote_count: 4590,
  //   },
  //   {
  //     poster_path:
  //       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
  //     overview:
  //       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
  //     release_date: '23-08-2019',
  //     id: '123',
  //     original_title: 'Joker',
  //     title: 'Joker',
  //     popularity: 12,
  //     vote_average: 7.6,
  //     vote_count: 4590,
  //   },
  //   {
  //     poster_path:
  //       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
  //     overview:
  //       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
  //     release_date: '23-08-2019',
  //     id: '123',
  //     original_title: 'Joker',
  //     title: 'Joker',
  //     popularity: 12,
  //     vote_average: 7.6,
  //     vote_count: 4590,
  //   },
  //   {
  //     poster_path:
  //       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
  //     overview:
  //       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
  //     release_date: '23-08-2019',
  //     id: '123',
  //     original_title: 'Joker',
  //     title: 'Joker',
  //     popularity: 12,
  //     vote_average: 7.6,
  //     vote_count: 4590,
  //   },
  //   {
  //     poster_path:
  //       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
  //     overview:
  //       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
  //     release_date: '23-08-2019',
  //     id: '123',
  //     original_title: 'Joker',
  //     title: 'Joker',
  //     popularity: 12,
  //     vote_average: 7.6,
  //     vote_count: 4590,
  //   },
  //   {
  //     poster_path:
  //       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
  //     overview:
  //       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
  //     release_date: '23-08-2019',
  //     id: '123',
  //     original_title: 'Joker',
  //     title: 'Joker',
  //     popularity: 12,
  //     vote_average: 7.6,
  //     vote_count: 4590,
  //   },
  //   {
  //     poster_path:
  //       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
  //     overview:
  //       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
  //     release_date: '23-08-2019',
  //     id: '123',
  //     original_title: 'Joker',
  //     title: 'Joker',
  //     popularity: 12,
  //     vote_average: 7.6,
  //     vote_count: 4590,
  //   },
  //   {
  //     poster_path:
  //       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
  //     overview:
  //       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
  //     release_date: '23-08-2019',
  //     id: '123',
  //     original_title: 'Joker',
  //     title: 'Joker',
  //     popularity: 12,
  //     vote_average: 7.6,
  //     vote_count: 4590,
  //   },
  // ];
  pageIndex = 0;
  showFirstLastButtons = true;
  pageSizeOptions = [20];

  moviesData: Movie[] = [];

  constructor(
    private moviesService: TopMoviesService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.moviesService
      .fetchPopularMovies(1)
      .subscribe((movies) => (this.moviesData = movies));
  }

  handlePageEvent(event: PageEvent) {
    let index = event.pageIndex + 1;
    this.pageIndex = index;
    this.moviesService.fetchPopularMovies(index).subscribe((data) => {
      this.moviesData = data;
    });
  }

  onMovieClicked(movie: Movie) {
    this.router.navigate(['movie', movie.id]);
  }
}
