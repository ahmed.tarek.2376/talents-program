import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  email!: FormControl;
  password!: FormControl;
  loginForm!: FormGroup;
  displayWarning = false;
  hide = true;

  constructor(private router: Router, private authService: AuthService) {
    if (this.authService.isAuthenticated()) router.navigate(['']);
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }
    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  ngOnInit(): void {
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.password = new FormControl('', [
      Validators.required,
      Validators.minLength(1),
    ]);

    this.loginForm = new FormGroup({
      username: this.email,
      password: this.password,
    });
  }

  login() {
    let successful = this.authService.login(
      new User(this.email.value, this.password.value)
    );
    if (successful) {
      this.router.navigate(['/']);
    } else {
      this.displayWarning = true;
    }
  }

  navigateHome() {
    this.router.navigate(['']);
  }
}
