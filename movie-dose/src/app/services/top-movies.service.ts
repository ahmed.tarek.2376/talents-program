import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Movie } from '../shared/movie.model';
import { Injectable } from '@angular/core';

// const EXAMPLE_DATA: Movie[] = [
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.6,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 3.9,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.0,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.9,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.9,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.9,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.9,
//   },
//   {
//     poster_path:
//       'https://i.pinimg.com/originals/eb/b9/40/ebb940a5d3e9f77d9b11878d8005e8a2.jpg',
//     overview:
//       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
//     release_date: '2019',
//     id: '123',
//     original_title: 'Joker',
//     title: 'Joker',
//     popularity: 12,
//     vote_average: 7.9,
//   },
// ];

@Injectable()
export class TopMoviesService {
  private apiKey = '127ef8102efbb5a087762506ab8cabc2';
  popularURL = 'https://api.themoviedb.org/3/movie/popular';

  data: Movie[] = [];
  totalPopularMovies = 0;

  constructor(private http: HttpClient) {}

  fetchPopularMovies(page: number) {
    return this.http
      .get<{
        page: number;
        results: [Movie];
        total_results: number;
        total_pages: number;
      }>(this.popularURL, {
        params: new HttpParams().set('api_key', this.apiKey).set('page', page),
      })
      .pipe(
        map((res) => {
          this.totalPopularMovies = res.total_results;
          const moviesArray: Movie[] = [];
          console.log(res.results);
          for (const index in res.results) {
            moviesArray.push({
              ...res.results[index],
              poster_path:
                'https://image.tmdb.org/t/p/w500' +
                res.results[index].poster_path,
              popularity: Number(index) + 1 + (page - 1) * 20,
              vote_average: res.results[index].vote_average * 10,
            });
          }
          return moviesArray;
        })
      );
  }
}
